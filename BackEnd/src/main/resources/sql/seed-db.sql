SET SCHEMA js;

INSERT INTO js.student VALUES
(1, 'maykool jimenez', parsedatetime('01-01-1985', 'dd-mm-yyyy'), 'maykoljb@gmail.com'),
(2, 'minor madrigal', parsedatetime('01-01-1985', 'dd-mm-yyyy'), 'minormmb@gmail.com'),
(3, 'bryan jimenez', parsedatetime('20-02-2002', 'dd-mm-yyyy'), 'bryanjb@gmail.com')