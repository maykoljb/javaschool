CREATE SCHEMA IF NOT EXISTS js;
SET SCHEMA js;

DROP TABLE js.students IF EXISTS;
CREATE TABLE js.student (
  id        INTEGER PRIMARY KEY,
  name      VARCHAR(300) NOT NULL,
  birthday  date NOT NULL,
  email     VARCHAR(50) NOT NULL
);