package com.javaschool.repository;

import com.javaschool.entity.Student;

import java.util.List;

public interface StudentRepository {
    List<Student> getStudentsByTeacher();
}
