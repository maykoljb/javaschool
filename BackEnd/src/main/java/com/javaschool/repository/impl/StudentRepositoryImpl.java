package com.javaschool.repository.impl;

import com.javaschool.repository.StudentRepository;
import com.javaschool.entity.Student;
import com.javaschool.ocr.HibernateSessionFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentRepositoryImpl implements StudentRepository {

    @Override
    public List<Student> getStudentsByTeacher() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        List<Student> students = session.createQuery("from Student").list();
        session.close();
        return students;
    }
}
