package com.javaschool.controller;

import com.javaschool.dto.StudentDto;
import com.javaschool.service.impl.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api")
public class StudentsController {

    @Autowired
    private StudentServiceImpl studentService;

    @RequestMapping("/student")
    public StudentDto getStudent(int id){
        return studentService.getStudent();
    }

    @RequestMapping("/students")
    public List<StudentDto> getStudents(){
        return studentService.getStudents();
    }
}
