package com.javaschool.service;

import com.javaschool.dto.StudentDto;

import java.util.List;

public interface StudentService {
    StudentDto getStudent();
    List<StudentDto> getStudents();
}
