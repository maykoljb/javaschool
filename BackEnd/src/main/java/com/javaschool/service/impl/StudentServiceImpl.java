package com.javaschool.service.impl;

import com.javaschool.ModelMapper.AutoMapper;
import com.javaschool.dto.StudentDto;
import com.javaschool.entity.Student;
import com.javaschool.repository.StudentRepository;
import com.javaschool.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public StudentDto getStudent() {
        return new StudentDto(1, "Maykool", new Date());
    }

    @Override
    public List<StudentDto> getStudents() {
        List<Student> studentsList = studentRepository.getStudentsByTeacher();
        return studentsList.stream().map(s -> AutoMapper.mapper.map(s, StudentDto.class)).collect(Collectors.toList());
    }
}
