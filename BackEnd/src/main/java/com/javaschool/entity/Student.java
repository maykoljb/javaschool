package com.javaschool.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Student {
    private int id;
    private String name;
    private Date birthday;
    private String email;


    @Id
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false, length = 300)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Temporal(TemporalType.DATE)
    @Column(name = "birthday")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Column(name = "email", nullable = false, length = 50)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
