package com.javaschool.ModelMapper;

import com.javaschool.dto.StudentDto;
import com.javaschool.entity.Student;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

public final class AutoMapper {
    public static ModelMapper mapper = new ModelMapper();

    private AutoMapper() {
        mapper.addMappings(new PropertyMap<Student, StudentDto>() {
                                    @Override
                                    protected void configure() {
                                        map().setId(source.getId());
                                        map().setName(source.getName());
                                        map().setBirthday(source.getBirthday());
                                    }
                                }

        );
    }
}