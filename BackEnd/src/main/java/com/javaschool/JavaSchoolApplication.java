package com.javaschool;

import com.javaschool.ocr.HibernateSessionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class JavaSchoolApplication {

    static private ConfigurableApplicationContext ctx;

    public static void main(String[] args) {
        ctx = SpringApplication.run(JavaSchoolApplication.class, args);
    }

    public static void stop(String[] args) {
        HibernateSessionFactory.shutdown();
        System.exit(SpringApplication.exit(ctx));
    }

    public static void stopContext(String[] args) {
        HibernateSessionFactory.shutdown();
        ctx.close();
    }
}
