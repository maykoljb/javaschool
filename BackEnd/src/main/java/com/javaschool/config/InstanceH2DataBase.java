package com.javaschool.config;

import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class InstanceH2DataBase {

    // jdbc:h2:mem:testdb
    @Bean(destroyMethod = "shutdown")
    public DataSource dataSource() {
        EmbeddedDatabase db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .setName("js")
                .addScripts("sql/create-db.sql")
                .addScripts("sql/seed-db.sql")
                .build();
        return db;
    }


    // Start WebServer, access http://localhost:8082
    @Bean(destroyMethod = "stop")
    public Server startH2WebManager() throws SQLException {
        Server dbServer = Server.createWebServer("-webPort", "8081");
        dbServer.start();
        return dbServer;
    }
}